# Aura - Unreal Gameplay Ability 
This project is my personal learning depot for the Udemy course "[Unreal Engine 5 - Gameplay Ability System - Top Down RPG](https://www.udemy.com/course/unreal-engine-5-gas-top-down-rpg)". 

# TakeAway
## Main Parts of Gameplay Ability System:
1. `Ability System Component`:\
    It is a controller of GAS attached on actor. You can activate/cancel ability with this compoennt. If you don't have this component, gameplay effect doesn't work.
2. `Attribute Set`:\
    It is a set of attributes. ex: strength, intelligence, health...

3. `Gameplay Ability`:\
    a class we used to encapsulate functionnality that things or characters can do. like Attack, Cast spells. Ex: you want to do "nova", it can do damage, slow attack speed, slow movement speed. These three functionalities are well encapsulated in the class Nova.

4. `Ability Task`:\
    Run async code for `Gameplay Ability`. You can do a task across a period of time with it or finish it immediately as you want. It is like a worker to perform a work.

5. `Gameplay Effect`:\
    It is used to change values of `Attribute Set`. Instant, Duration, or Infinite.

6. `Gameplay Cue`:\
    It is called to play sfx, vfx.


7. `Gameplay Tag`:\
    Same as layer in unity OR tag in unreal, but more structured. You can access it by calling Enemy.Attack.InstantDamage

## Ability System Component Setup

Ability System Component and Attribute Set should be on the same layer. But the parent depends on your needs.


> Root ->
    >> Ability System Component\
    >> Attribut Set

1. Setup with `Pawn`\
It is a component of pawn. When pawn is destroyed, the `Ability System Component` is destroyed as well.

2. Setup with `Player State`\
It is a component of player state, independant of other component. When the pawn is destroyed, we keep the Ability System Component.



## Player State
```NetUpdateFrequency = 100.f```: how often the server updates clients.

## Multiplayer

Server is the Authority in this project.

* Dedicated Server:
    1. No huam player
    2. No rendering to a screen
* Listen Server:
    1. Is a human player
    2. No lag on host



### Game Mode (Server):
You have only 1 Game Mode instance on server, it includes rules of the game, spawner of players, restart of the game...

### Player Controller (Server and Client):
Server has everyone's player controller, but client has his own player controller.
So that server can calculate and send back the right value on this client.

### Player State (Server and Client)
Both Server and Client has everyone's player state.

### HUD and Widgets (Client)
UI and HUD exists only on client.

### Replication Mode

| Replication Mode    | Use Case | Description 
| ------------------- | -------- | ----------
| Full    | Single Player    | Gameplay Effects are replicated to all clients
| Mixed   | Multiplayer, Player-Controlled     | Gameplay Effects are replicated to the owning clients only. Gameplay Cues and Gameplay Tags are replicated to all clients.
| Minimal | Multiplayer, AI- Controlled    | Gameplay Effects are NOT replicated. Gameplay Cues and Gameplay Tags are replicated to all clients. 

For `Mixed Replication Mode`: The `OwnerActor`'s Owner must be the Controller. For Pawns, this is et automatically in PossessedBy(). \
The player State's Owner is automatically set to the Controller. \
THerefore, if you `OwnerActor` is not the PlayerState, and you use Mixed Repplicaiton Mode, you must call `SetOwner()` on the `OwnerActor` to set its owner to the controller.
