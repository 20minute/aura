// Fill out your copyright notice in the Description page of Project Settings.


#include "Input/AuraInputConfig.h"

const UInputAction* UAuraInputConfig::FindAbilityInputActionForTag(const FGameplayTag& InputTag, bool bLogNotFound) const
{
	for(const FAuraInputAction& AuraInputAction : AbilityInputActions)
	{
		if(AuraInputAction.InputAction && AuraInputAction.InputTag.MatchesTagExact(InputTag))
		{
			return AuraInputAction.InputAction;
		}
	}
	
	if(bLogNotFound)
	{
		UE_LOG(LogTemp, Error, TEXT("Can't find AbilityInputAction for InputTag[%s], on InputAction [%s]"), *InputTag.ToString(), *GetName());
	}

	return nullptr;
}
