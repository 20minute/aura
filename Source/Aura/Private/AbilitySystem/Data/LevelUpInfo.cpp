// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitySystem/Data/LevelUpInfo.h"

int32 ULevelUpInfo::FindLevelForXP(int32 XP) const
{
	for (int32 i = 1; i < LevelUpInformation.Num(); i++)
	{
		if (XP <= LevelUpInformation[i].LevelUpRequirement)
		{
			return i;
		}
	}
	return 1;
}
